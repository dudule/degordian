const burger = document.getElementsByClassName('header__burger');
const navMenu = document.getElementsByClassName('header__nav');
const header = document.getElementsByClassName("header");

burger[0].addEventListener('click', function () {
    // toggleMenu();
    toggleMenuLink();
    toggleX();
});
function toggleMenuLink() {
    navMenu[0].classList.toggle('slide-in');
}
function toggleX() {
    burger[0].classList.toggle('active');
    document.querySelectorAll('body')[0].classList.toggle('overflow');
}

if (screen.width <= 800) {
    const prevScrollpos = window.pageYOffset;
    window.onscroll = function () {
        const currentScrollPos = window.pageYOffset;
        if (prevScrollpos > currentScrollPos) {
            header[0].style.top = "0";
        } else {
            header[0].style.top = "-67px";
        }
        prevScrollpos = currentScrollPos;
    }
}